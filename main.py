from Player import *
from Stone import *


screen = pygame.display.set_mode((WIDTH, HEIGHT))
player = Player()
PLAYERS.add(player) # создали игрока

for i in range(STONE_COUNT):
    STONES.add(Stone())  # создаем камни и добавляем в группу камней
SPRITES.add(STONES, PLAYERS) # игрока и все камни добавляем во все спрайты
running = True
while running:
    CLOCK.tick(FPS)
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            running = False
        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_SPACE:
                BULLETS.add(Bullet(player.rect.centerx, player.rect.centery))
                SPRITES.add(BULLETS)

    if pygame.sprite.groupcollide(STONES, PLAYERS, True, True):
        running = False
    hits = pygame.sprite.groupcollide(STONES, BULLETS, True, True)
    for hit in hits:
        SCORE += 1
        print(SCORE)
        stone = Stone()
        STONES.add(stone)
        SPRITES.add(stone)

    SPRITES.update()
    screen.fill(WHITE)
    SPRITES.draw(screen)
    pygame.display.flip()
pygame.quit()
