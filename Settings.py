import pygame
import random
import os
pygame.init()
pygame.mixer.init()

# Настройки путей к файлам
GAME_FOLDER = os.path.dirname(__file__)
IMAGES = os.path.join(GAME_FOLDER, 'images')

# Настройки игры
WIDTH = 1000
HEIGHT = 800
FPS = 30
SPRITES = pygame.sprite.Group() # группа всех спрайтов
CLOCK = pygame.time.Clock()
pygame.display.set_caption("python Game")

# Цвета игры
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)

# Игрок
PLAYER_IMG = pygame.image.load(os.path.join(IMAGES, 'rocket.png'))
PLAYER_SPEED = 7
PLAYERS = pygame.sprite.Group()

# Камни
STONE_IMG = pygame.image.load(os.path.join(IMAGES, 'stone.png'))
STONE_COUNT = 25
STONES = pygame.sprite.Group() # группа всех камней
STONE_SPEED = 1

# Пули
BULLETS = pygame.sprite.Group()
BULLET_SPEED = 25

SCORE = 0
