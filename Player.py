from Settings import *
from Bullet import *

class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = PLAYER_IMG
        self.rect = self.image.get_rect()
        self.radius = 45
        self.rect.centerx = WIDTH / 2
        self.rect.bottom = HEIGHT - 30
        self.speedx = 0
        self.speedy = 0

    def update(self):
        self.speedx = self.speedy = 0
        key = pygame.key.get_pressed()
        if key[pygame.K_RIGHT]:
            self.speedx = PLAYER_SPEED
        if key[pygame.K_LEFT]:
            self.speedx = -PLAYER_SPEED
        if key[pygame.K_UP]:
            self.speedy = -PLAYER_SPEED
        if key[pygame.K_DOWN] and self.rect.bottom < HEIGHT - 30:
            self.speedy = PLAYER_SPEED
        self.rect.x += self.speedx
        self.rect.y += self.speedy

        if self.rect.left > WIDTH:
            self.rect.right = 0
        if self.rect.right < 0:
            self.rect.left = WIDTH
