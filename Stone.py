from Settings import *

class Stone(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = STONE_IMG
        self.rect = self.image.get_rect()
        self.radius = 30
        self.rect.x = random.randrange(WIDTH - self.rect.width)
        self.rect.y = random.randrange(-200, -self.rect.height)
        self.speedy = random.randrange(1, 5) * STONE_SPEED
        self.speedx = random.randrange(-1, 1) * STONE_SPEED

    def update(self):
        self.rect.x += self.speedx
        self.rect.y += self.speedy
        if self.rect.top > HEIGHT + 10 or self.rect.left < -25 or self.rect.right > WIDTH + 20:
            self.rect.x = random.randrange(WIDTH - self.rect.width)
            self.rect.y = random.randrange(-200, -self.rect.height)
            self.speedy = random.randrange(1, 5) * STONE_SPEED
